The Power of Review
===================

This repository contains the materials for *The Power of Review* workshop.

This repository uses Git submodules to refer to the various different language
implementations of *docopt* which we use for the system-under-review. To clone
use the ``--recursive`` option::

  $ git clone --recursive git@bitbucket.org:sixty-north/power-of-review-workshop.git 


Instructions
------------

Instructions for the code inspection exericse are provided in ``exercise.pdf``.
